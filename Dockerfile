FROM debian:bookworm-slim
RUN apt-get update \
    && apt-get install --no-install-recommends -y \
        ca-certificates \
        curl \
    && rm -rf /var/lib/apt/lists/*
COPY dist/ /usr/bin/
USER 1000
