workflow:
  rules:
    # Build merge request events
    - if: "$CI_PIPELINE_SOURCE == 'merge_request_event'"
    # Prevent duplicate pipelines on pushes to branches with open merge requests
    - if: "$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS"
      when: never
    # Always build the default branch
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"
    # Build any other branch or tag
    - if: "$CI_COMMIT_BRANCH || $CI_COMMIT_TAG"


stages:
  - linter
  - test
  - build
  - release


variables:
  CARGO_HOME: ${CI_PROJECT_DIR}/.cargo
  CARGO_TERM_COLOR: always

###
linter:lint:
  stage: linter
  image: rust:1.74-slim-bookworm
  before_script:
    - rustc --version
    - cargo --version
  script:
    - rustup component add clippy rustfmt
    - cargo clippy -- -D warnings
    - cargo fmt -- --check
  cache: &package_cache
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - .cargo/bin
      - .cargo/registry/index
      - .cargo/registry/cache
      - target/debug/deps
      - target/debug/build
      - target/release/deps
      - target/release/build
    policy: pull-push
  interruptible: true

###
build:cargo:
  stage: build
  image: rust:1.74-slim-bookworm
  script:
    - mkdir dist
    - if [ "${CI_COMMIT_BRANCH}" = "${CI_DEFAULT_BRANCH}" ] || [ -n "${CI_COMMIT_TAG}" ] ; then
        cargo build --bins --release --verbose ;
        cp target/release/gltools-* dist/ ;
      else
        cargo build --bins --verbose ;
        cp target/debug/gltools-* dist/ ;
      fi
    - rm dist/*.d
  cache:
    <<: *package_cache
  artifacts:
    paths:
      - dist/
    when: on_success
    expire_in: "30 days"
  needs:
    - linter:lint
  interruptible: true

build:kaniko:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  before_script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"https://gitlab.com\":{\"auth\":\"$(echo -n gitlab-ci-token:${CI_JOB_TOKEN} | base64)\"},\"https://registry.gitlab.com\":{\"auth\":\"$(echo -n gitlab-ci-token:${CI_JOB_TOKEN} | base64)\"}}}" > /kaniko/.docker/config.json
    - DESTINATIONS="--destination ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}"
    # Only push the `latest` image on merge to the default branch.
    # If we push the tag each time, broken builds images may end up in places
    # where they shouldn't because they assume `latest` to be a stable image
    # from the default branch.
    - if [ "${CI_COMMIT_BRANCH}" = "${CI_DEFAULT_BRANCH}" ] ; then
        DESTINATIONS="${DESTINATIONS} --destination ${CI_REGISTRY_IMAGE}:latest";
      fi
    # When we're running on a git tag, we should also tag the image accordingly
    # so we can use it again later on.
    - if [ -n "${CI_COMMIT_TAG}" ] ; then
        DESTINATIONS="${DESTINATIONS} --destination ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}";
      fi
  script:
    # Build the actual image
    - >
      /kaniko/executor
      --build-arg "CI_JOB_TOKEN=${CI_PACKAGE_CONTAINER_REGISTRY}"
      --cache
      --cleanup
      --context "${CI_PROJECT_DIR}"
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile"
      ${DESTINATIONS}
  variables:
    GODEBUG: "http2client=0"
  rules:
    - when: on_success
  needs:
    - job: build:cargo
      artifacts: true
  interruptible: true

###
release:upload:
  stage: release
  image: curlimages/curl:latest
  variables:
    PACKAGE_REGISTRY_URL: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/gltools/${CI_COMMIT_TAG}
  script:
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "dist/gltools-commentator" "${PACKAGE_REGISTRY_URL}/gltools-commentator"
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "dist/gltools-committer" "${PACKAGE_REGISTRY_URL}/gltools-committer"
  needs:
    - job: build:cargo
      artifacts: true
    - build:kaniko
  rules:
    - if: $CI_COMMIT_TAG

release:publish:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  variables:
    PACKAGE_REGISTRY_URL: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/gltools/${CI_COMMIT_TAG}
  script:
    - echo "Releasing ${CI_COMMIT_TAG}"
  needs:
    - release:upload
  rules:
    - if: $CI_COMMIT_TAG
  release:
    tag_name: $CI_COMMIT_TAG
    name: 'Release $CI_COMMIT_TAG'
    description: 'Release created using the release-cli.'
    assets:
      links:
        - name: gltools-commentator
          filepath: /gltools-commentator
          url: "${PACKAGE_REGISTRY_URL}/gltools-commentator"
          link_type: package
        - name: gltools-committer
          filepath: /gltools-committer
          url: "${PACKAGE_REGISTRY_URL}/gltools-committer"
          link_type: package
