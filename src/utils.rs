use std::env;
use std::process::exit;

use gitlab::Gitlab;

pub fn envvar(key: &str) -> String {
    match env::var(key) {
        Ok(t) => t,
        Err(_) => {
            eprintln!("Missing environment variable '{}'.", key);
            exit(1);
        }
    }
}

pub fn envvar_u64(key: &str) -> u64 {
    let var = envvar(key);
    match var.parse() {
        Ok(i) => i,
        Err(_) => {
            eprintln!(
                "Environment variable '{}' has value '{}' which is not an integer.",
                key, var
            );
            exit(1);
        }
    }
}

pub fn get_client() -> Gitlab {
    let token = envvar("GITLAB_TOKEN");
    Gitlab::new("gitlab.com", token).unwrap()
}
