use std::env;
use std::io;
use std::io::Read;

use gitlab::api::projects::merge_requests::notes;
use gitlab::api::{self, paged, users, Pagination, Query};
use serde::Deserialize;

mod utils;

#[derive(Debug, Deserialize, PartialEq)]
struct User {
    id: u64,
}
#[derive(Debug, Deserialize)]
struct MergeRequestNote {
    id: u64,
    body: String,
    author: User,
}

pub fn envvar_default(key: &str, default: &str) -> String {
    env::var(key).unwrap_or(default.to_string())
}

fn main() {
    let project_id: u64 = utils::envvar_u64("CI_MERGE_REQUEST_PROJECT_ID");
    let comment_mark: String = envvar_default("COMMENTATOR_MARK", "gl-mr-commentator");
    let mr_id: u64 = utils::envvar_u64("CI_MERGE_REQUEST_IID");
    let client = utils::get_client();

    let marker = format!("<!-- {} -->", comment_mark);
    println!("Merge request marker is '{}'", marker);

    let mut content = marker.clone();
    content.push('\n');
    io::stdin().read_to_string(&mut content).unwrap();
    content += r#"
---

This comment was made by the [GitLab Tools Merge Request Commentator](https://gitlab.com/MarkusH/gltools)."#;

    let endpoint = users::CurrentUser::builder().build().unwrap();
    let current_user: User = endpoint.query(&client).unwrap();

    let endpoint = notes::MergeRequestNotes::builder()
        .project(project_id)
        .merge_request(mr_id)
        .order_by(notes::NoteOrderBy::CreatedAt)
        .sort(api::common::SortOrder::Ascending)
        .build()
        .unwrap();
    let notes: Vec<MergeRequestNote> = paged(endpoint, Pagination::All).query(&client).unwrap();

    println!("Found a total of {} merge request notes.", notes.len());

    let mut note: Option<MergeRequestNote> = None;
    for n in notes {
        if n.author != current_user {
            continue;
        }
        if !n.body.starts_with(&marker) {
            continue;
        }
        note = Some(n);
        break;
    }

    match note {
        Some(n) => {
            println!("Updating merge request comment {}", n.id);
            let endpoint = notes::EditMergeRequestNote::builder()
                .project(project_id)
                .merge_request(mr_id)
                .note(n.id)
                .body(content)
                .build()
                .unwrap();
            let _: () = api::ignore(endpoint).query(&client).unwrap();
        }
        None => {
            println!("Creating merge request comment ...");
            let endpoint = notes::CreateMergeRequestNote::builder()
                .project(project_id)
                .merge_request(mr_id)
                .body(content)
                .build()
                .unwrap();
            let _: () = api::ignore(endpoint).query(&client).unwrap();
        }
    }
}
